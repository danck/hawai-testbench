package testbench

import (
	"fmt"
	"golang.org/x/net/websocket"
	"io"
	"log"
)

const channelBufferSize = 100

var maxId int = 0

type wsClient struct {
	id     int
	ws     *websocket.Conn
	server *wsServer
	ch     chan *Message
	doneCh chan bool
}

func NewClient(ws *websocket.Conn, server *wsServer) *wsClient {
	if ws == nil {
		panic("ws is nil")
	}

	if server == nil {
		panic("wsServer is nil")
	}

	maxId++
	ch := make(chan *Message, channelBufferSize)
	doneCh := make(chan bool)

	return &wsClient{maxId, ws, server, ch, doneCh}
}

func (c *wsClient) Conn() *websocket.Conn {
	return c.ws
}

func (c *wsClient) Write(msg *Message) {
	select {
	case c.ch <- msg:
	default:
		c.server.Del(c)
		err := fmt.Errorf("client %d is disconnected", c.id)
		c.server.Err(err)
	}
}

func (c *wsClient) Done() {
	c.doneCh <- true
}

func (c *wsClient) Listen() {
	go c.listenWrite()
	c.listenRead()
}

func (c *wsClient) listenWrite() {
	log.Println("Listening write to client")
	for {
		select {
		case msg := <-c.ch:
			log.Println("Send: ", msg)
			websocket.JSON.Send(c.ws, msg)

		case <-c.doneCh:
			c.server.Del(c)
			c.doneCh <- true
			return
		}
	}
}

func (c *wsClient) listenRead() {
	log.Println("Listening read from client")
	for {
		select {
		case <-c.doneCh:
			c.server.Del(c)
			c.doneCh <- true
			return

		default:
			var msg Message
			err := websocket.JSON.Receive(c.ws, &msg)
			if err == io.EOF {
				c.doneCh <- true
			} else if err != nil {
				c.server.Err(err)
			} else {
				c.server.sendAll(&msg)
			}
		}
	}
}
