package testbench

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

// subscribe registers this application at the services given below
func initSubscribers() error {
	subscribeRegistry()
	if err := subscribeLogging(); err != nil {
		return err
	}

	return nil
}

type registryStatus struct {
}

const maxNumberRetries = 30
const pollingInterval = 1 // seconds

// subscribeRegistry starts actively polling the registry for its status and
// pushes the result into the 'regStats' channel
func subscribeRegistry() {
	retries := 0 //number of attempts to reconnect since last success
	ticker := time.NewTicker(time.Millisecond * 1000 * pollingInterval)
	go func() {
		for _ = range ticker.C {
			if retries >= maxNumberRetries {
				break
			}
			resp, err := http.Get(*registryAddress)
			if err != nil {
				log.Printf("Error while getting registry status: %s", err.Error())
				retries += 1
				continue
			}

			var regStatus registryStatus
			decoder := json.NewDecoder(resp.Body)
			err = decoder.Decode(&regStatus)
			if err != nil {
				log.Printf("Error while unmarshalling registry status: %s", err.Error())
				retries += 1
				continue
			}

			retries = 0
			regStats <- regStatus
		}
		log.Println("Connection to service registry: maximal number of retries exceeded, giving up!")
	}()
}

// subscribeLogging registers this service as a logging subscriber
func subscribeLogging() error {
	return nil
}
