package testbench

import (
	"flag"
	"log"
	"net/http"
)

var (
	listenAddress = flag.String(
		"listen-addr",
		":8080",
		"Address to listen on")
	registryAddress = flag.String(
		"registry-address",
		"http://localhost:32000",
		"Address and port of the service registry")
)

var (
	regStats chan registryStatus
	logMsgs  chan string //logMessage
)

func Main() {
	flag.Parse()

	//initSubscribers()

	// websocket handler to push messages to the browser app
	//http.Handle("/ws", websocket.Handler(wsHandler))

	//ws := NewWSServer("/ws")
	//go ws.Listen()
	// http handler to receive requests from the browser app
	//http.Handle("/orders", errorHandler(ordersHandler))

	// http handler to receive log messages
	//http.Handle("/subscriptions/logging", errorHandler(loggingHandler))

	// http handler to serve the browser app
	http.Handle("/", http.FileServer(http.Dir("static")))

	log.Printf("Starting to listen on %s", *listenAddress)
	log.Fatal(http.ListenAndServe(*listenAddress, nil))
}
