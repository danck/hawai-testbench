package testbench

import (
	"net/http"
)

func ordersHandler(w http.ResponseWriter, r *http.Request) error {
	return nil
}

type message struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

//func wsHandler(ws *websocket.Conn) {
//	openSocket <- ws
//	defer func() { closeSocket <- ws }()
//	ticker := time.NewTicker(time.Millisecond * 1000)
//	for _ = range ticker.C {
//		data := message{"log", "bongo"}
//		websocket.JSON.Send(ws, data)
//		//todo(danck): check for connection status -> break if disconnected
//		log.Println("Tick")
//	}
//}

// loggingHandler accepts messages from the loggingHub and pushes them into
// the appropriate channel
func loggingHandler(w http.ResponseWriter, r *http.Request) error {
	return nil
}
