var tablist = ['logging', 'registry'];

function activateTab(id) {
	document.getElementById("title").innerHTML = id.toUpperCase();
	for (i in tablist){
		var tab = tablist[i];
		var elem = document.getElementById(tab);
		elem.style.display = 'none';
	}

	for (i in tablist) {
		var tab = tablist[i];
		var elem = document.getElementById(tab);
		if (tab == id) {
			elem.style.display = 'block';
		} 
	}
}
