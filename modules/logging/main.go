package main

import (
	"flag"
	"log"
	"net/http"
)

var (
	listenAddress = flag.String(
		"listen-addr",
		":32005",
		"Address to listen on")
	registryURL = flag.String(
		"registry-url",
		"http://localhost:32000",
		"Address and port of the service registry")
	pollingInterval = flag.Int(
		"polling-interval",
		1,
		"Polling interval in seconds")
	loggingServiceLabel = flag.String(
		"logging-service-label",
		"logging",
		"Servicelabel of the logging service")
)

var (
	ws *wsServer // websocket server
)

func main() {
	log.SetFlags(log.Lshortfile)
	flag.Parse()

	// websocket server to push messages to the browser app
	ws = NewWSServer("/ws")
	go ws.Listen()

	// http handler to serve the static files
	http.Handle("/", http.FileServer(http.Dir("static")))

	// poll the registry status and send it to the clients
	//	http handler to receive log messages from the hub
	http.HandleFunc("/receiver", receiverHandler)

	log.Printf("Creating new resolver with URL", *registryURL)
	rsv := NewResolver(*registryURL)
	loggingURL, err := rsv.Resolve(*loggingServiceLabel)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Trying to subscribe")
	subscribe(loggingURL+"/sub/logging", "http://192.168.29.143:32005/receiver")

	log.Printf("Starting to listen on %s", *listenAddress)
	log.Fatal(http.ListenAndServe(*listenAddress, nil))
}
