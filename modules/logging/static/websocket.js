var endpoint = "ws://localhost:32005/ws";
var ws;

connect(endpoint);

function connect(endpoint) {
	var ws = new WebSocket(endpoint);

	ws.onclose = function() {
		setTimeout(function(){connect(endpoint)}, 5000)
	};

	ws.onmessage = function(event) {
		var msg = JSON.parse(event.data);
		console.log("[LOGGING]"+msg.type)

		switch(msg.type) {
			case "log":
				text = msg.text;
				appendLog(text);
				break;
			case "logmessages":
				appendLogMessage(msg);
				break;
		}
	};
}

function appendLogMessage(msg) {
	var msgcontainer = document.getElementById('logs');
	var newMsg = document.createTextNode(msg.body +"\n");
	msgcontainer.appendChild(newMsg);
	msgcontainer.scrollTop = msgcontainer.scrollHeight;
};

function appendLog(text) {
	console.log(text);
};
