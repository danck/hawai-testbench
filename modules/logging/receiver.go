package main

import (
	"io/ioutil"
	//"encoding/json"
	"log"
	"net/http"
)

type message struct {
	Type string `json:"type"`
	Body string `json:"body"`
}

func receiverHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("receiver called")
	var body []byte
	//decoder := bytes.NewDecoder(r.Body)
	//err := decoder.Decode(&body)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err.Error())
	}
	defer r.Body.Close()
	msg := Message{
		Type: "logmessages",
		Body: string(body),
	}

	log.Println("Sending:", msg.Body)
	ws.SendAll(&msg)
}
