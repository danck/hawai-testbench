package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"time"
)

type Resolver struct {
	registry   *url.URL
	retries    int
	maxRetries int
}

// One-off type to parse registry response
type services struct {
	List []*service `json:"services"`
}

type service struct {
	Address string `json:"address"`
}

func NewResolver(registry string) *Resolver {
	url, err := url.Parse(registry)
	if err != nil {
		log.Fatal("Illegal registry URL:", err)
	}
	return &Resolver{
		registry:   url,
		retries:    0,
		maxRetries: 10,
	}
}

func (r *Resolver) Resolve(service string) (string, error) {
	var serviceURL string
	var err error

	for {
		serviceURL, err = r.tryResolve(service)
		if err == nil {
			break
		}
		log.Println("Error while resolving:", err.Error())
		time.Sleep(time.Second)
	}
	return serviceURL, err
}

func (r *Resolver) tryResolve(service string) (string, error) {
	resp, err := http.Get(r.registry.String() + "/service/" + service)
	if err != nil {
		log.Println("Unable to contact registry. Retry #", r.retries, err.Error())
		r.retries++
		return "", err
	}
	defer resp.Body.Close()

	//sadf, _ := ioutil.ReadAll(resp.Body)
	//log.Fatal(string(sadf))

	var serviceList services
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&serviceList)
	if err != nil {
		log.Println("Unable to decode response from the registry", err.Error())
		return "", err
	}

	if len(serviceList.List) == 0 {
		return "", errors.New("Service not available: " + service)
	}

	log.Print(fmt.Sprintf("%s", serviceList.List[0].Address))

	return fmt.Sprintf("%s", serviceList.List[0].Address), nil
}
