package main

import (
	"encoding/json"
	"log"
)

type Message struct {
	Type string      `json:"type"`
	Body interface{} `json:"body"`
}

func (m *Message) String() string {
	str, err := json.Marshal(m)
	if err != nil {
		log.Println("Unable to parse message")
		return ""
	}

	return string(str[:])
}
