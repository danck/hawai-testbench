package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func subscribe(remoteURL string, localURL string) {
	req, err := http.NewRequest("POST", remoteURL, strings.NewReader(localURL))
	if err != nil {
		log.Println(err.Error())
		return
	}

	client := &http.Client{}
	log.Print(remoteURL)
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err.Error())
		return
	}
	defer resp.Body.Close()

	log.Printf("Getting response")
	res, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Println("Subscription to logging hub yields:", string(res))
}
