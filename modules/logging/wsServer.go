package main

import (
	"log"
	"net/http"

	"golang.org/x/net/websocket"
)

const bufferSize = 32

type wsServer struct {
	prefix    string
	messages  []*Message
	clients   map[int]*wsClient
	addCh     chan *wsClient
	delCh     chan *wsClient
	sendAllCh chan *Message
	doneCh    chan bool
	errCh     chan error
}

func NewWSServer(prefix string) *wsServer {
	messages := []*Message{}
	clients := make(map[int]*wsClient)
	addCh := make(chan *wsClient, bufferSize)
	delCh := make(chan *wsClient, bufferSize)
	sendAllCh := make(chan *Message, bufferSize)
	doneCh := make(chan bool)
	errCh := make(chan error)

	return &wsServer{
		prefix,
		messages,
		clients,
		addCh,
		delCh,
		sendAllCh,
		doneCh,
		errCh,
	}
}

func (s *wsServer) Add(c *wsClient) {
	s.addCh <- c
}

func (s *wsServer) Del(c *wsClient) {
	s.delCh <- c
}

func (s *wsServer) SendAll(m *Message) {
	s.sendAllCh <- m
}

func (s *wsServer) Done() {
	s.doneCh <- true
}

func (s *wsServer) Err(err error) {
	s.errCh <- err
}

func (s *wsServer) sendPastMessages(c *wsClient) {
	for _, msg := range s.messages {
		c.Write(msg)
	}
}

func (s *wsServer) sendAll(msg *Message) {
	for _, c := range s.clients {
		c.Write(msg)
	}
}

func (s *wsServer) Listen() {
	log.Println("Start listening on websocket")

	onConnected := func(ws *websocket.Conn) {
		defer func() {
			err := ws.Close()
			if err != nil {
				s.errCh <- err
			}
		}()

		client := NewClient(ws, s)
		s.Add(client)
		client.Listen()
	}
	http.Handle(s.prefix, websocket.Handler(onConnected))
	log.Println("Handler created")

	for {
		select {

		case c := <-s.addCh:
			s.clients[c.id] = c
			log.Println("Currently", len(s.clients), "clients connected")
			s.sendPastMessages(c)

		case c := <-s.delCh:
			delete(s.clients, c.id)
			log.Println("Currently", len(s.clients), "clients connected")

		case msg := <-s.sendAllCh:
			log.Println("Broadcasting to clients:", msg)
			s.messages = append(s.messages, msg)
			s.sendAll(msg)

		case err := <-s.errCh:
			log.Println("Error:", err.Error())

		case <-s.doneCh:
			return
		}
	}
}
