package main

import (
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"time"
)

var (
	listenAddress = flag.String(
		"listen-addr",
		":32004",
		"Address to listen on")
	registryURL = flag.String(
		"registry-url",
		"http://localhost:32000",
		"Address and port of the service registry")
	pollingInterval = flag.Int(
		"polling-interval",
		1,
		"Polling interval in seconds")
)

var (
	ws *wsServer // websocket server
)

func main() {
	flag.Parse()

	// websocket server to push messages to the browser app
	ws = NewWSServer("/ws")
	go ws.Listen()

	// http handler to serve the static files
	http.Handle("/", http.FileServer(http.Dir("static")))

	// poll the registry status and send it to the clients
	go pollRegistryStatus()

	log.Printf("Starting to listen on %s", *listenAddress)
	log.Fatal(http.ListenAndServe(*listenAddress, nil))
}

func pollRegistryStatus() {
	ticker := time.NewTicker(time.Second * 1)
	for {
		<-ticker.C
		resp, err := http.Get(*registryURL)
		if err != nil {
			log.Println("Unable to fetch registry status: ", err.Error())
			continue
		}
		var respBody interface{}
		decoder := json.NewDecoder(resp.Body)
		err = decoder.Decode(&respBody)
		if err != nil {
			log.Println("Unable to read response by the registry: ", err.Error())
			resp.Body.Close()
			continue
		}

		ws.SendAll(&Message{
			Type: "registryStatus",
			Body: respBody,
		})
		resp.Body.Close()
	}
}
