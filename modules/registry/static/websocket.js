var endpoint = "ws://localhost:32004/ws";

connect(endpoint);

function connect(endpoint) {
	var ws = new WebSocket(endpoint);

	// retry if the connection gets lost
	ws.onclose = function() {
		setTimeout(function(){connect(endpoint)}, 5000)
	};

	// dispatch incoming messages
	ws.onmessage = function(event) {
		var msg = JSON.parse(event.data);
		console.log(msg.type)

		switch(msg.type) {
			case "log":
				text = msg.text;
				appendLog(text);
				break;
			case "registryStatus":
				refreshRegistryStatus(event.data);
				break;
		}
	};
}

// rebuild tables as new status messages come in
function refreshRegistryStatus(text) {
	console.log(text)
	var msgcontainer = document.getElementById('regstats');
	var stats = JSON.parse(text);
	var services = stats.body.Entries;
	var beginTable = '<table class="pure-table" style="width:100%;">' +
		"<thead><tr>" +
		"<th>Endpoint</th>" +
		"<th>Last seen</th>" +
		"<th>ID</th>" +
		"</tr></thead>" +
		"<tbody>";
	var endTable = "</tbody></table>";	

	var out = ""

	for (service in services) {
		console.log("handling service "+service)
		out += '<h3 class="pure-u-1-5">' + service.toUpperCase() + "</h3>";
		out += '<div class="pure-u-5-5">'
		out += beginTable;
		for (endpoint in services[service]) {
			out += "<tr>";
			out += "<td>"+services[service][endpoint].address+"</td>";
			out += "<td>"+services[service][endpoint]["last-seen"]+"</td>";
			out += "<td>"+services[service][endpoint].id+"</td>";
			out += "</tr>";
		}
		out += endTable;
		out += "</div>";
	}

	msgcontainer.innerHTML = out;
};

function appendLog(text) {
	console.log(text);
};
